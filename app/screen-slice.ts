import { createSlice} from '@reduxjs/toolkit';

const screenSlice = createSlice({
    name: 'screen',
    initialState: {
        screenName: 'Main',
        mainScreenId: 0,
        userSelected: 0,
        userSelectedObj: {},
    },
    reducers: {
        setScreenName: (state, action) => {
            state.screenName = action.payload;
        },
        setMainScreenId: (state, action) => {
            state.mainScreenId = action.payload;
        },
        setUserSelected: (state, action) => {
            state.userSelected = action.payload;
        },
        setUserSelectedObj: (state, action) => {
            state.userSelectedObj = action.payload;
        }
    }   
})

export const {setScreenName, setMainScreenId, setUserSelected, setUserSelectedObj} = screenSlice.actions;
export default screenSlice.reducer;