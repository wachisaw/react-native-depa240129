import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth-slice";
import screenReducer from "./screen-slice";

export default configureStore({
    reducer: {
        //สร้าง auth-slice.ts ก่อน แล้วค่อยเอามาใส่ในนี้
        auth: authReducer,
        screen: screenReducer
    },
})

