//data structure ที่เก็บข้อมูล

import { createSlice} from '@reduxjs/toolkit';

const authSlice = createSlice({
    name: 'auth',

     //ข้างในเก็บค่า state
    initialState: {
        isLogedIn: false,
        apiToken: '',
    },

     //function ที่เรียกหาแล้วมันจะไป set ค่า state ข้างบนให้เรา
    reducers: {
        //ในแต่ละ function จะรับ parm คือ state กับ action(optional)
        logIn: (state) => {
            state.isLogedIn = true;
            // state.apiToken = ...
        },
        logOut: (state) => {
            state.isLogedIn = false;
            state.apiToken = '';
        },
        setKey: (state, action) => {
            state.apiToken = action.payload;
        }
    }   
})

export const {logIn, logOut, setKey} = authSlice.actions;
export default authSlice.reducer;