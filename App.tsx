
import { FC } from 'react';
import { SafeAreaView, View, Text } from 'react-native';
import { Login } from './components/screen/Login'
import { Login2 } from './components/screen/Login2'
import { Main } from './components/screen/Main'
import { ScreenManage } from './components/screen/ScreenManage'
import { PlayArea } from './components/game/PlayArea'
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

//redux
import { Provider } from 'react-redux'
import store from './app/store'

// const App: FC = () => { //fc = function component
  
//   return (
//     <SafeAreaView 
//     >
//       <View>
//       <Login></Login>
//       {/* <Text>xxx</Text> */}
//       </View>
     
//     </SafeAreaView>
//   );
// }

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          {/* <Stack.Screen
            name="Login"
            component={Login}
            options={{title: 'Login'}}
          /> */}
          <Stack.Screen
            name="ScreenManage"
            component={ScreenManage}
            options={{title: 'ScreenManage'}}
          />
          <Stack.Screen
            name="Main"
            component={Main}
            options={{title: 'Main'}}
          />
          <Stack.Screen
            name="PlayArea"
            component={PlayArea}
            options={{title: 'PlayArea'}}
          />
          
          <Stack.Screen
            name="Login2"
            component={Login2}
            options={{title: 'Login2'}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;


