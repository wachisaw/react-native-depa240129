
import { FC, useState } from 'react';
import { Text, View, Button, Alert, StyleSheet, SafeAreaView } from 'react-native';
import { BigText } from './components/shared/BigText';
import { MyButton } from './components/shared/MyButton';
// import { MMKV } from 'react-native-mmkv'
// import GlobalStyles from './GlobalStyles'; // not work

// export const storage = new MMKV()

const App: FC = () => { //fc = function component
  let counter = 0;
  const showName = (num: number) => {
    return "number = " + num;
  }
  const [count, setCount] = useState(0);
  const [formdata, setFormData] = useState({
    cnt: 0, docno: ""
  });

  
  const whenAdd = async () => {
    //Alert.alert('Add');
    //setCount(count + 1);

    formdata.cnt++;
    formdata.docno = "ID-"+formdata.cnt;
    console.log(formdata);
    setFormData({...formdata});
    // storage.set('formdata', JSON.stringify(formdata))
  }

  // const whenLoad = async () => {
  //     const jsonUser  = storage.getString('formdata');
  //     const object = JSON.parse(jsonUser );
  //     setFormData({...object});
  // }

  return (
    <SafeAreaView 
    // style={GlobalStyles.droidSafeArea}
    >
      <View>
        <Text>xxx</Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text>Text1</Text>
        <BigText text="Line3"></BigText>
        <Text style={[styles.btn, styles.btnPrimary]}>Hello World {showName(formdata.cnt)} {formdata.docno}</Text>
        <Text style={[styles.btn]}>Hello World {showName(count)}</Text>
        <Button title="Add 1" onPress={whenAdd}></Button>
        <View className="flex flex-row gap-2 mt-2 ml-2 mr-7"> 
        {
          //gap-2 จะมีผลกับ View ที่อยู่ด้านใน
        }
          <View className="w-1/2">
            <MyButton label="Add 2" whenPress={whenAdd}></MyButton>
          </View>
          <View className="w-1/2">
            <MyButton label="Add 3" whenPress={whenAdd}></MyButton>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: '#eaeaea',
  },
  title: {
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: '#20232a',
    borderRadius: 6,
    backgroundColor: '#61dafb',
    color: '#20232a',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  btn: {
    display: 'flex',
    paddingVertical: 6,
    paddingHorizontal: 12,
    marginVertical: 0,
    fontSize: 14,
    fontWeight: '400',
    textAlign: 'center',
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 4,
  },
  btnPrimary: {
    color: '#fff',
    backgroundColor: '#337ab7',
    borderColor: '#2e6da4',
  },
  body: {
    fontFamily: 'Helvetica Neue, Helvetica, Arial, sans-serif',
    fontSize: 14,
    color: '#333',
  },
  h1: {
    fontSize: 36,
    fontWeight: '500',
  },
  h2: {
    fontSize: 30,
    fontWeight: '500',
  },
});

export default App;


//teacher use vs code extension -------------
// Tailwind CSS IntelliSense












//comment by teacher
// import { StatusBar } from 'expo-status-bar';
// import { StyleSheet, Text, View } from 'react-native';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.tsx to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
