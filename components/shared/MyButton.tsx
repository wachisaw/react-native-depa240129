import { FC } from 'react';
import { Pressable, Text } from 'react-native';

interface Props {
    label: string,
    whenPress?: () => void,
    addiClassName?: string
}
  
//onPress={props.onKod}
export const MyButton: React.FC<Props> = (props) => {
    return <>
        <Pressable className={"border bg-blue-400 p-2 active:bg-blue-500 rounded-md border-blue-500 "+props.addiClassName }
            onPress={props.whenPress}>
            <Text className="text-white text-center">{props.label}</Text>
        </Pressable>
    </>
}