import { FC } from 'react'
import { View, Text, Pressable, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

interface Props {
    label: string,
    iconName: any,
    iconColor: string,
    iconSize?: number,
    whenPress?: () => void
}

export const MyIconButton: FC<Props> = (props) => {
    
    return <>
        <Pressable className=" active:bg-blue-200 items-center" onPress={props.whenPress}>
            <Ionicons className="item-center" name={props.iconName} size={props.iconSize ?? 32} color={props.iconColor} />
            <Text className="text-center" style={styles.menuText}>{props.label}</Text>
        </Pressable>
    </>
}

const styles = StyleSheet.create({
    menuText: {
      color: "#ffd480",
      fontSize: 15,
      fontWeight: 'bold'
    },
  });