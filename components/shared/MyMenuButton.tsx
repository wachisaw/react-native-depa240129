import { FC } from 'react'
import { View, Text, Pressable, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

interface Props {
    label: string,
    iconName: any,
    iconColor: string,
    iconSize?: number,
    whenPress?: () => void
}

export const MyMenuButton: FC<Props> = (props) => {
    
    return <>
        <Pressable className="active:bg-blue-200" onPress={props.whenPress}>
                <View className="items-center border p-2" style={styles.menubuttonbg}>
                    <Ionicons className="item-center" name={props.iconName} size={props.iconSize ?? 96} color={props.iconColor} />
                    <Text className="text-xl" style={styles.menuText}>{props.label}</Text>
                </View>
        </Pressable>
    </>
}


const styles = StyleSheet.create({
    menubuttonbg: {
    //   backgroundColor: "#d6d3c0",
      backgroundColor: "#fff",
    //   borderColor: "#b5a785",
    //   borderColor: "#abd6ff",
      borderRadius: 15,
      borderLeftWidth: 4,
      borderTopWidth: 2,
      borderRightWidth: 4,
      borderBottomWidth: 9,
      borderLeftColor: "#abd6ff",
      borderTopColor: "#abd6ff",
      borderRightColor: "#abd6ff",
      borderBottomColor: "#8ccfff",
    },
    menuText: {
        color:"#787878",
        fontWeight:'bold',
    }
  });
