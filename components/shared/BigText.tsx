import { FC } from 'react';
import { Text, View } from 'react-native';


interface Props {
    text: string,
}
  
export const BigText: React.FC<Props> = (props) => {
    return <>
        <Text className="text-3xl text-center text-emerald-500">{props.text}-1</Text>
        <View className="flex flex-row items-center space-x-0">
            <Text className="basis-1/4 bg-gray-100 text-left">{props.text}-2</Text>
            <Text className="basis-1/4 bg-gray-200 text-right">{props.text}-3</Text>
        </View>
    </>
}