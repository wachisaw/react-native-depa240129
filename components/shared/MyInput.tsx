import { FC } from 'react';
import { View,Text,TextInput,StyleSheet } from 'react-native';
import { Controller, Control } from "react-hook-form";

interface Props {
    control: Control<any>,
    label: string,
    name: string,
    secureTextEntry?: boolean,
}

export const MyInput: FC<Props> = (props) => {
    return <>
        <View className="mt-5">
            <Text>{props.label}</Text>
        </View>
        <View>
            {/* <TextInput className="border border-gray-300" 
                style={styles.textinput}
                {...register('email')}
               ></TextInput> */}
                <Controller 
                control={props.control}
                name={props.name}
                render={({field}) =>
                (
                  <TextInput className="border border-gray-300" 
                  secureTextEntry={props.secureTextEntry}
                  style={styles.textinput}
                  value={field.value}
                  onChangeText={field.onChange}
                  ></TextInput>
                )
                }
              />
        </View>
    </>
}

const styles = StyleSheet.create({
    textinput: {
      borderStyle: "solid",
      borderColor: "#ccc",
      backgroundColor: "#fff"
    },
  });

export default MyInput;