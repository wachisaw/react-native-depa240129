import { FC } from 'react'
import { Text, StyleSheet } from 'react-native'
import { Login } from './Login'
import { Main } from './Main'
import { User } from './User'
import { ToDo } from './ToDo'
import { Photo } from './Photo'
import { useSelector } from 'react-redux'

export const ScreenManage: FC = () => {
    const logedIn = useSelector((s: any) => s.auth.isLogedIn)   //เมื่อ useSelector เมื่อไหร่ ค่าในตัวแปร logedIn เปลี่ยน มันจะมา render ใหม่เองเลย
    const screenName = useSelector((s: any) => s.screen.screenName)   

    const screenShowing = () => {
        // console.log(111, logedIn, screenName)
        if (! logedIn) {
            return <Login/>
        } else if (screenName === "User") {
            return <User/>
        } else if (screenName === "ToDo") {
            return <ToDo/>
        } else if (screenName === "Photo") {
            return <Photo/>
        } else {
            return <Main/>
        }
    }
    return <>
        {/* {! logedIn ?
            <Login />
            :
            <Main />
        } */}

        {/* {! logedIn && <Login />}
        {logedIn && <Main />} */}

        {screenShowing()}

    </>
}


const styles = StyleSheet.create({
    init: {
      marginTop: 200
    },
  });