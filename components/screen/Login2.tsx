
import { FC, useState, useEffect } from 'react';
import { Text, View, Button, Alert, StyleSheet, SafeAreaView, TextInput } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import { useRoute } from '@react-navigation/native';

export const Login2: FC = () => { 

  const navigation = useNavigation();
  const route = useRoute();

  const [formdata, setFormData] = useState({
    email:"", password:""
  });
  const [formtempdata, setFormTempData] = useState({
    message:""
  });
  const localStorageName = "my-key-loginform";

  const whenLogin = async () => {

    // formdata.cnt++;
    // formdata.docno = "ID-"+formdata.cnt;
    // setFormData({...formdata});

    // formdata.email = "";
    // formdata.password = "";
    formtempdata.message = "Login fail";
    setFormTempData({...formtempdata})
    storeLocal(localStorageName, formdata)
  }

  const storeLocal = async (localStorageName:any, obj: any) => {
    try {
      const jsonValue = JSON.stringify(obj);
      await AsyncStorage.setItem(localStorageName, jsonValue);
    } catch (e) {
      // saving error
    }
  } 
  
  const whenReset = async () => {
    formdata.email = "";
    formdata.password = "";
    formtempdata.message = "";
    setFormData({...formdata})
    setFormTempData({...formtempdata})
    storeLocal(localStorageName, formdata)
  }

  useEffect(() => { 
    // formdata.email = "aaa";
    getData(localStorageName).then((obj) => {
      console.log(obj)
      setFormData({...obj})
    });
    // setFormData({...formdata})

    console.log('route-param', route.params)
  }, []);

  const getData = async (localStorageName: any) => {
    try {
      const jsonValue = await AsyncStorage.getItem(localStorageName);
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      // error reading value
    }
  };



  return (
      <View className="flex flex-col h-screen p-4 bg-gray-100">
        <View className="mt-20 items-center">
            <Text className="text-lg">Login 2</Text>
        </View>
        <View className="mt-5">
            <Text>Email</Text>
        </View>
        <View>
            <TextInput className="border border-gray-300" 
                style={styles.textinput}
                value={formdata.email}
                onChangeText={(text) => { formdata.email = text; setFormData({...formdata}); }}></TextInput>
        </View>
        <View className="mt-5">
            <Text>Password</Text>
        </View>
        <View>
            <TextInput className="border border-gray-300" 
                secureTextEntry
                style={styles.textinput}
                value={formdata.password}
                onChangeText={(text) => { formdata.password = text; setFormData({...formdata}); }}></TextInput>
        </View>
        <View className="flex flex-row gap-2 mt-2 ml-2 mr-7"> 
          <View className="w-1/2">
            <Button title="Login" onPress={whenLogin}></Button>
          </View>
          <View className="w-1/2">
            <Button title="Reset" onPress={whenReset}></Button>
          </View>
        </View>
        {/* <View>
            <Text>{formdata.email+", "+formdata.password}</Text>
        </View> */}
        <View className="mt-10">
            <Text style={styles.message}>{formtempdata.message}</Text>
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  textinput: {
    borderStyle: "solid",
    borderColor: "#ccc",
    backgroundColor: "#fff"
  },
  message: {
    color: "red",
  }
});

export default Login2;
