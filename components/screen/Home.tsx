import { FC } from 'react'
import { SafeAreaView, Text, View, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'

export const Home: FC = (props) => {
    
    return <>
        <SafeAreaView>
            <View className="items-center mt-10">
                <Ionicons className="item-center" name="cloud" size={96} color="#fff" />
                <Text className="text-center" style={styles.homeText}>My Application</Text>
            </View>
        </SafeAreaView>
    </>
}

const styles = StyleSheet.create({
    homeText: {
      color: "#379ad4",
      fontSize: 20,
      fontWeight: 'bold'
    },
  });