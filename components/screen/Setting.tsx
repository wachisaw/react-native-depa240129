import { FC } from 'react'
import { Text, View } from 'react-native'
import { MyButton } from '../shared/MyButton'
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { logOut } from '../../app/auth-slice'

export const Setting: FC = (props) => {
    
    const dispatch = useDispatch();
    const auth = useSelector((s: any) => s.auth);
    const whenLogout = () => {
        dispatch(logOut());
    }

    return <>
        <Text className="text-center">Setting</Text>
        <View>
            <Text>Your key : {auth.apiToken}</Text>
        </View>
        <MyButton label='logout' whenPress={whenLogout} />
    </>
}