
import { FC, useState, useEffect } from 'react';
import { Text, View, Button, Alert, StyleSheet, SafeAreaView, TextInput, ScrollView, Pressable  } from 'react-native';
import { Ionicons } from '@expo/vector-icons'
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import { z } from 'zod';
// import { Controller, SubmitErrorHandler, SubmitHandler, useForm } from "react-hook-form";
// import { zodResolver } from "@hookform/resolvers/zod";
// import { useNavigation } from '@react-navigation/native';
// import { MyInput } from '../shared/MyInput';

//redux
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux'
import { setScreenName, setUserSelected, setUserSelectedObj } from '../../app/screen-slice'


export const User: FC = () => { 

  const apiToken = useSelector((s: any) => s.auth.apiToken);
  const dispatch = useDispatch();

  const [data, setData] = useState([]);
  // const navigation = useNavigation();

  const fetchData = async () => {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/users'); 
      const jsonData = await response.json();
      // console.log(jsonData)
      setData(jsonData);
    } catch (error) {
      console.error('เกิดข้อผิดพลาดในการร้องขอข้อมูล:', error);
    }
  };




  useEffect(() => { 
    fetchData();

  }, []);


  return (
      <View className="flex flex-col h-screen p-4" style={styles.view1}>
        <View style={styles.header1}>
          <View className="flex flex-row mt-2 ml-4 items-center">
              <Ionicons className="item-center" name="person-circle" size={36} />
              <Text className="ml-2 text-lg" style={styles.titleHeader}>USER</Text>
          </View>
          <View className="flex flex-row gap-2 mt-2 ml-2 mr-7 items-right" style={styles.closeButtonPanel1}> 
            <View className="w-1/1">
              <Pressable onPress={() => dispatch(setScreenName('Main'))}>
                <Text style={styles.closeButton1}>X</Text>
              </Pressable>
            </View>
          </View>
        </View>
        <ScrollView style={styles.container}>
          {data &&
            data.map((item: any, index) => (
              <Pressable key={index} onPress={()=>{
                  // console.log(item.id)
                  dispatch(setUserSelected(item.id));
                  dispatch(setUserSelectedObj({id:item.id, name:item.name}));
                  dispatch(setScreenName("ToDo"));
              }}>
              <View style={[styles.row, styles.menubuttonbg]}>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Name: </Text>
                  <Text style={styles.label2}>{item.name}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Username: </Text>
                  <Text style={styles.label2}>{item.username}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Email: </Text>
                  <Text style={styles.label2}>{item.email}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Street: </Text>
                  <Text style={styles.label2}>{item.address.street}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Suite: </Text>
                  <Text style={styles.label2}>{item.address.suite}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>City: </Text>
                  <Text style={styles.label2}>{item.address.city}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Zipcode: </Text>
                  <Text style={styles.label2}>{item.address.zipcode}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Phone: </Text>
                  <Text style={styles.label2}>{item.phone}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Website: </Text>
                  <Text style={styles.label2}>{item.website}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Company Name: </Text>
                  <Text style={styles.label2}>{item.company.name}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Company CatchPhrase: </Text>
                  <Text style={styles.label2}>{item.company.catchPhrase}</Text>
                </View>
                <View style={styles.viewrow}>
                  <Text style={styles.label1}>Company BS: </Text>
                  <Text style={styles.label2}>{item.company.bs}</Text>
                </View>
              </View>
              </Pressable>
            ))}
        </ScrollView>
              <View><Text></Text></View>
              <View><Text></Text></View>
              <View><Text></Text></View>
              <View><Text></Text></View>
              
      </View>
      
  );
}

const styles = StyleSheet.create({
  viewrow:{
    flex:1, flexDirection:'row'
  },
  titleHeader: {
    fontWeight: 'bold',
    color: '#67acc9',
    fontSize: 25
  },
  view1: {
    backgroundColor: '#d4f2ff',
  },
  header1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  closeButton1: {
    fontWeight:'bold',
    fontSize: 20,
    color: 'gray',
    textAlign: 'right'
  },
  closeButtonPanel1: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  textinput: {
    borderStyle: "solid",
    borderColor: "#ccc",
    backgroundColor: "#fff"
  },
  message: {
    color: "red",
  },
  container: {
    flex: 1,
    padding: 16,
  },
  row: {
    marginBottom: 16,
    padding: 16,
    
  },
  label1: {
    fontSize: 13,
    marginBottom: 8,
    fontWeight:'bold',
    width: '30%',
    color: 'gray'
  },
  label2: {
    fontSize: 16,
    marginBottom: 8,
    width: '70%',
  },
  menubuttonbg: {
      backgroundColor: "#fff",
      borderRadius: 15,
      borderLeftWidth: 2,
      borderTopWidth: 2,
      borderRightWidth: 2,
      borderBottomWidth: 9,
      borderLeftColor: "#abd6ff",
      borderTopColor: "#abd6ff",
      borderRightColor: "#abd6ff",
      borderBottomColor: "#8ccfff",
    },
    menuText: {
        color:"#787878",
        fontWeight:'bold',
    }
});


export default User;

/*
teacher install
npm install react-hook-form
npm install @hookform/resolvers
npm install zod
*/