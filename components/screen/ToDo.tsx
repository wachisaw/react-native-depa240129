
import { FC, useState, useEffect } from 'react';
import { Text, View, Button, Alert, StyleSheet, SafeAreaView, TextInput, ScrollView, Pressable  } from 'react-native';
import { Ionicons } from '@expo/vector-icons'
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import { z } from 'zod';
// import { Controller, SubmitErrorHandler, SubmitHandler, useForm } from "react-hook-form";
// import { zodResolver } from "@hookform/resolvers/zod";
// import { useNavigation } from '@react-navigation/native';
// import { MyInput } from '../shared/MyInput';

//redux
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux'
import { setScreenName } from '../../app/screen-slice'


export const ToDo: FC = () => { 

  const apiToken = useSelector((s: any) => s.auth.apiToken);
  const userSelected = useSelector((s: any) => s.screen.userSelected);
  const userSelectedObj = useSelector((s: any) => s.screen.userSelectedObj);
  const dispatch = useDispatch();

  const [data, setData] = useState([]);
  // const navigation = useNavigation();

  const fetchData = async () => {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/todos'); 
      const jsonData = await response.json();
      // console.log(jsonData)
      setData(jsonData);
    } catch (error) {
      console.error('เกิดข้อผิดพลาดในการร้องขอข้อมูล:', error);
    }
  };




  useEffect(() => { 
    fetchData();

  }, []);

  const listItems = () => {
    const items:any = [];
    data.forEach((item:any, index) => {
      if (userSelected === 0 || item.userId === userSelected) {
        const itemView = (
          <View key={item.id} style={[styles.row, styles.menubuttonbg]}>
            {/* <View style={{flex:1, flexDirection:'row'}}>
              <Text style={styles.label1}>User ID: </Text>
              <Text style={styles.label2}>{item.userId}</Text>
            </View> */}
            <View style={styles.viewrow}>
              <Text style={styles.label1}>ID: </Text>
              <Text style={styles.label2}>{item.id}</Text>
            </View>
            <View style={styles.viewrow}>
              <Text style={styles.label1}>Title: </Text>
              <Text style={styles.label2}>{item.title}</Text>
            </View>
            <View style={styles.viewrow}>
              <Text style={styles.label1}>Completed: </Text>
              <Text style={styles.label2}>{item.completed ? 'Yes' : 'No'}</Text>
            </View>
          </View>
        );
        items.push(itemView); // เพิ่ม itemView เข้าไปใน array items
      }
    });

    return <>{items}</>;
  }


  return (
      <View className="flex flex-col h-screen p-4" style={styles.view1}>
        <View className="flex flex-row mt-2 ml-4 items-center">
              <Ionicons className="item-center" name="person-circle" size={36} />
              <Text className="ml-2 text-lg" style={styles.titleHeader2}>{userSelectedObj.name}</Text>
          </View>
        <View style={styles.header1}>
          
          <View className="flex flex-row mt-2 ml-4 items-center">
              <Ionicons className="item-center" name="list" size={36} />
              <Text className="ml-2 text-lg" style={styles.titleHeader}>To do list</Text>
          </View>
          
          <View className="flex flex-row gap-2 mt-2 ml-2 mr-7 items-right" style={styles.closeButtonPanel1}> 
            <View className="w-1/1">
              <Pressable onPress={() => {
                if (userSelected === 0) {
                  dispatch(setScreenName('Main'))
                } else {
                  dispatch(setScreenName('User'))
                }
              }}>
                <Text style={styles.closeButton1}>X</Text>
              </Pressable>
            </View>
          </View>
        </View>
        <ScrollView style={styles.container}>
          {listItems()}
          {/* {data.map((item:any, index) => ( */}
        </ScrollView>
              <View><Text></Text></View>
              <View><Text></Text></View>
              <View><Text></Text></View>
              <View><Text></Text></View>
              
      </View>
      
  );
}

const styles = StyleSheet.create({
  viewrow:{
    flex:1, flexDirection:'row'
  },
  titleHeader: {
    fontWeight: 'bold',
    color: '#67acc9',
    fontSize: 25
  },
  titleHeader2: {
    fontWeight: 'bold',
    color: 'gray',
    fontSize: 20
  },
  view1: {
    backgroundColor: '#d4f2ff',
  },
  header1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  closeButton1: {
    fontWeight:'bold',
    fontSize: 20,
    color: 'gray',
    textAlign: 'right'
  },
  closeButtonPanel1: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  textinput: {
    borderStyle: "solid",
    borderColor: "#ccc",
    backgroundColor: "#fff"
  },
  message: {
    color: "red",
  },
  container: {
    flex: 1,
    padding: 16,
  },
  row: {
    marginBottom: 16,
    padding: 16,
    
  },
  label1: {
    fontSize: 13,
    marginBottom: 8,
    fontWeight:'bold',
    width: '30%',
    color: 'gray'
  },
  label2: {
    fontSize: 16,
    marginBottom: 8,
    width: '70%',
  },
  menubuttonbg: {
      backgroundColor: "#fff",
      borderRadius: 15,
      borderLeftWidth: 2,
      borderTopWidth: 2,
      borderRightWidth: 2,
      borderBottomWidth: 9,
      borderLeftColor: "#abd6ff",
      borderTopColor: "#abd6ff",
      borderRightColor: "#abd6ff",
      borderBottomColor: "#8ccfff",
    },
    menuText: {
        color:"#787878",
        fontWeight:'bold',
    }
});


export default ToDo;

/*
teacher install
npm install react-hook-form
npm install @hookform/resolvers
npm install zod
*/