import { FC } from 'react'
import { Text, View, SafeAreaView, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { MyMenuButton } from '../shared/MyMenuButton'

import { useDispatch } from 'react-redux';
import { setScreenName, setUserSelected, setUserSelectedObj } from '../../app/screen-slice'
import { useSelector } from 'react-redux'

export const Menu: FC = (props) => {

    const dispatch = useDispatch();

    return <>
        <SafeAreaView>
            <View className="flex flex-row flex-wrap p-2" style={styles.menuBackground}>
                <View className='w-1/3 p-1'>
                    <MyMenuButton label="User" iconName="person-circle"
                        iconColor="#ffbb00"
                        iconSize={77}
                        whenPress={() => {
                            dispatch(setScreenName('User'))
                        }}
                    />
                </View>
                <View className='w-1/3 p-1'>
                    <MyMenuButton label="Todo" iconName="list"
                        iconColor="#ffbb00"
                        iconSize={77}
                        whenPress={() => {
                            dispatch(setUserSelected(0));
                            dispatch(setUserSelectedObj({id:0, name:"All"}));
                            dispatch(setScreenName('ToDo'))
                        }}
                    />
                </View>
                <View className='w-1/3 p-1'>
                    <MyMenuButton label="Photo" iconName="image"
                        iconColor="#ffbb00"
                        iconSize={77}
                        whenPress={() => {
                            dispatch(setScreenName('Photo'))
                        }}
                    />
                </View>
                <View className='w-1/3 p-1'>
                    <MyMenuButton label="Album" iconName="albums"
                        iconColor="#ffbb00"
                        iconSize={77}
                        whenPress={() => {}}
                    />
                </View>
                <View className='w-1/3 p-1'>
                    <MyMenuButton label="Comment" iconName="chatbox"
                        iconColor="#ffbb00"
                        iconSize={77}
                        whenPress={() => {}}
                    />
                </View>
                <View className='w-1/3 p-1'>
                    <MyMenuButton label="Post" iconName="create"
                        iconColor="#ffbb00"
                        iconSize={77}
                        whenPress={() => {}}
                    />
                </View>
            </View>
        </SafeAreaView>
    </>
}

const styles = StyleSheet.create({
    menuBackground: {
      backgroundColor: "#d4f2ff"
    },
  });