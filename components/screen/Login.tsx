
import { FC, useState, useEffect } from 'react';
import { Text, View, Button, Alert, StyleSheet, SafeAreaView, TextInput } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { z } from 'zod';
import { Controller, SubmitErrorHandler, SubmitHandler, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useNavigation } from '@react-navigation/native';
import { MyInput } from '../shared/MyInput';

//redux
import { useDispatch } from 'react-redux';
import { logIn, setKey } from '../../app/auth-slice'

import { useSelector } from 'react-redux'

//เอาไว้สั่ง validate หน้า form ง่ายๆ
const LoginSchema = z.object({
  email: z.string().email(),
  password: z.string().min(3),
})

type LoginModel = z.infer<typeof LoginSchema>;  //นำมา declare เป็นรูปแบบของ json model




export const Login: FC = () => { 

  const apiToken = useSelector((s: any) => s.auth.apiToken);
  const dispatch = useDispatch();

  const navigation = useNavigation();

  const [formdata, setFormData] = useState({
    email:"", password:""
  });
  const [formtempdata, setFormTempData] = useState({
    message:""
  });
  const localStorageName = "my-key-loginform";

  
  const {control, register, handleSubmit } = useForm<LoginModel>({
    resolver: zodResolver(LoginSchema)
  })

  const validateSuccess: SubmitHandler<LoginModel> = (data) => {
    // console.log(data)
    // Alert.alert("Message", "Passed");
    dispatch(setKey('tokensample85739475')) //ของจริง token นี้ควรมาจาก api authen
    dispatch(logIn())
  }

  const validateFail: SubmitErrorHandler<LoginModel> = (errors) => {
    // console.log(errors.email?.message)
    // Alert.alert("Error", errors.root?.message);
    if (errors.email?.message !== undefined) {
      Alert.alert("Error", "Email "+errors.email?.message);
    } else
    if (errors.password?.message !== undefined) {
      Alert.alert("Error", "Password "+errors.password?.message);
    }
  }

  const whenLogin = async () => {
    // formtempdata.message = "Login fail";
    setFormTempData({...formtempdata})
    storeLocal(localStorageName, formdata)
    dispatch(setKey('tokensample85739475')) //ของจริง token นี้ควรมาจาก api authen
    dispatch(logIn())
  }

  const storeLocal = async (localStorageName:any, obj: any) => {
    try {
      const jsonValue = JSON.stringify(obj);
      await AsyncStorage.setItem(localStorageName, jsonValue);
    } catch (e) {
      // saving error
    }
  } 
  
  const whenReset = async () => {
    formdata.email = "";
    formdata.password = "";
    formtempdata.message = "";
    setFormData({...formdata})
    setFormTempData({...formtempdata})
    storeLocal(localStorageName, formdata)
  }

  useEffect(() => { 
    getData(localStorageName).then((obj) => {
      console.log(obj)
      setFormData({...obj})
    });

  }, []);

  useEffect(() => { 
    console.log(apiToken)
  }, [apiToken]);

  const getData = async (localStorageName: any) => {
    try {
      const jsonValue = await AsyncStorage.getItem(localStorageName);
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      // error reading value
    }
  };



  return (
      <View className="flex flex-col h-screen p-4 bg-gray-100">
        <View className="mt-20 items-center">
            <Text className="text-lg">Login</Text>
        </View>
        <View className="mt-5">
            <Text>Email</Text>
        </View>
        <View>
            <TextInput className="border border-gray-300" 
                style={styles.textinput}
                value={formdata.email}
                onChangeText={(text) => { formdata.email = text; setFormData({...formdata}); }}></TextInput>
             
        </View>
        <View>
              <MyInput 
                control={control}
                name="email"
                label="Email (zod)"
              />
        </View>
        <View className="mt-5">
            <Text>Password</Text>
        </View>
        <View>
            <TextInput className="border border-gray-300" 
                secureTextEntry
                style={styles.textinput}
                value={formdata.password}
                onChangeText={(text) => { formdata.password = text; setFormData({...formdata}); }}></TextInput>
              
        </View>
        <View>
              <MyInput 
                control={control}
                name="password"
                label="Password (zod)"
                secureTextEntry={true}
              />
        </View>
        <View className="flex flex-row gap-2 mt-2 ml-2 mr-7"> 
          <View className="w-1/2">
            <Button title="Login" onPress={whenLogin}></Button>
          </View>
          <View className="w-1/2">
            <Button title="Reset" onPress={whenReset}></Button>
          </View>
        </View>
        <View className="flex flex-row gap-2 mt-2 ml-2 mr-7"> 
          <View className="w-1/2">
            <Button title="Login (zod)" onPress={handleSubmit(validateSuccess, validateFail)}></Button>
          </View>
          <View className="w-1/2">
            <Button title="Reset (zod)" onPress={whenReset}></Button>
          </View>
        </View>
        {/* <View>
            <Text>{formdata.email+", "+formdata.password}</Text>
        </View> */}
        {formtempdata.message?
          <View className="mt-10">
              <Text>Message : </Text>
              <Text style={styles.message}>{formtempdata.message}</Text>
          </View>
          :
          <></>
        }
        <View className="mt-10">
          <Button title="Navigate" onPress={() => { 
            const dataToSend = { 
              key1: 'value1', 
              key2: 'value2' 
            };
            navigation.navigate('Login2', dataToSend); }}></Button>
        </View>
      </View>
      
  );
}

const styles = StyleSheet.create({
  textinput: {
    borderStyle: "solid",
    borderColor: "#ccc",
    backgroundColor: "#fff"
  },
  message: {
    color: "red",
  }
});

export default Login;

/*
teacher install
npm install react-hook-form
npm install @hookform/resolvers
npm install zod
*/