import { FC, useState } from 'react'
import { View, Text, Pressable, StyleSheet } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { MyIconButton } from '../shared/MyIconButton'
import { Home } from './Home'
import { Menu } from './Menu'
import { Message } from './Message'
import { Setting } from './Setting'

import { useDispatch } from 'react-redux';
import { setMainScreenId } from '../../app/screen-slice'
import { useSelector } from 'react-redux'

export const Main: FC = () => {
    
    const dispatch = useDispatch();

    const mainScreenId = useSelector((s: any) => s.screen.mainScreenId);


    // const [screenId, setScreenId] = useState(0);
    const displayScreen = () => {
        switch (mainScreenId) {
            case 0: return <Home />
            case 1: return <Menu />
            case 2: return <Message />
            case 3: return <Setting />
            default: return <Text>No component found !!!</Text>
        }
    }

    const changeScreenId = (screenId: number) => {
        // setScreenId(screenId); 
        dispatch(setMainScreenId(screenId))
    }

    return <>
        <View className="w-screen h-screen relative"  style={styles.menuBackground}>
            <View>{displayScreen()}</View>
            <View className="flex flex-row absolute" style={styles.bottomMenuBackground}>
                <View className="w-1/4">
                    <MyIconButton label='Home' iconName='home' iconColor='#fff' whenPress={() => changeScreenId(0)} />
                </View>
                <View className="w-1/4">
                    <MyIconButton label='Menu' iconName='menu' iconColor='#fff' whenPress={() => changeScreenId(1) } />
                </View>
                <View className="w-1/4">
                    <MyIconButton label='Message' iconName='chatbubbles' iconColor='#fff' whenPress={() =>changeScreenId(2)} />
                </View>
                <View className="w-1/4">
                    <MyIconButton label='Setting' iconName='settings' iconColor='#fff' whenPress={() =>changeScreenId(3)} />
                </View>
            </View>

            
        </View>
    </>
}

const styles = StyleSheet.create({
    menuBackground: {
      backgroundColor: "#d4f2ff"
    },
    bottomMenuBackground: {
        backgroundColor: "#119c13",
        bottom: 50,
        height: 90,
        paddingTop: 15, 
        borderTopWidth: 3,
        borderTopColor: '#278a2d'
    }
  });